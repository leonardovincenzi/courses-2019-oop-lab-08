package it.unibo.oop.lab.mvc;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class SimpleController implements Controller {

    private String nextString;
    private final List<String> stringHistory = new LinkedList<>();


    @Override
    public void setNextStringToPrint(final String nextString) {
        // TODO Auto-generated method stub
        this.nextString = Objects.requireNonNull(nextString, "This method does not accept null values.");

    }

    @Override
    public String getNextStringToPrint() {
        // TODO Auto-generated method stub
        return this.nextString;
    }

    @Override
    public final List<String> getPrintedStringHistory() {
        // TODO Auto-generated method stub
        return stringHistory;
    }

    @Override
    public void printCurrentString() {
        // TODO Auto-generated method stub
        if (this.nextString == null) {
            throw new IllegalStateException("There is no string set");
        }
        stringHistory.add(this.nextString);
        System.out.println(this.nextString);
    }

}
